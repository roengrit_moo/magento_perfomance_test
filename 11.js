import { check, group } from "k6";
import http from 'k6/http';
import { sleep } from 'k6';


// export const options = {
//   stages: [
//     { duration: "1m", target: 10 },
//     { duration: "3m", target: 10 },
//     { duration: "1m", target: 0 },
//   ],
//   ext: {
//     loadimpact: {
//       distribution: {
//         "amazon:sg:singapore": {
//           loadZone: "amazon:sg:singapore",
//           percent: 100,
//         },
//       },
//     },
//   },
// };

export function login(username, password, domain) {
  let response;
  let token;

  console.log('login');
  token = http.post(
    domain + "rest/V1/integration/customer/token",
    '{\r\n  "username": "' + username + '",\r\n  "password": "' + password + '"\r\n}'
    ,
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  check(token, {
    "status equals 200": response => response.status.toString() === "200",
  });
  if (token.status === 200) {
    //console.log(JSON.stringify(res.body));
    token = JSON.parse(token.body);
    console.log('token : ' + token);

  }


  return token;

}

export function createEmptyCart(token, domain) {
  let response;
  console.log('create_cart');

  group('Create Empty Cart', function () {
    token = http.post(
      domain + "rest/V1/carts/mine",
      null,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      }
    );
    check(token, {
      "status equals 200": response => response.status.toString() === "200",
    });
    if (token.status === 200) {
      token = JSON.parse(token.body);
      console.log('q_id : ' + token)
    }
  });
  sleep(2);
  return token;

}

export function addtocart(token, cart_id, sku, qty, domain) {
  let response;
  group('Add Product', function () {
    response = http.post(
      domain + "rest/V1/carts/mine/items/",
      '{"cartItem":{"sku":"' + sku + '","qty":' + qty + ',"quote_id":"' + cart_id + '"}}',
      {
        headers: {
          Authorization: "Bearer " + token,
          "content-type": "application/json",
        },
      }
    );
    check(response, {
      "status equals 200": response => response.status.toString() === "200",
    });
    sleep(2);
  });
}

export function payment(token, cart_id, domain) {
  let response;
  group('Save Data', function () {
    response = http.post(
      domain + "rest/th/V1/carts/mine/payment-information",
      `{"cartId":"` + cart_id + `","billingAddress":{"customerAddressId":"76","countryId":"TH","regionId":"913","regionCode":"UBN","region":"อุบลราชธานี","customerId":"62","street":["111/110"],"company":null,"telephone":"0837336436","fax":null,"postcode":"34280","city":"นาจะหลวย","firstname":"สมศรี","lastname":"วงศ์ศรีแก้ว","middlename":null,"prefix":null,"suffix":null,"vatId":null,"customAttributes":[{"attribute_code":"sub_district_id","value":"14833","label":"โสกแสง"},{"attribute_code":"city_id","value":"2752","label":"นาจะหลวย"},{"attribute_code":"sub_district","value":"โสกแสง"},{"attribute_code":"road","value":"สะอาดยิ่ง"},{"attribute_code":"soi","value":"ร่วมมิตร"},{"attribute_code":"moo","value":"12"},{"attribute_code":"floor","value":"8"},{"attribute_code":"separate_address","value":"1","label":"Billing"}],"extension_attributes":{"sub_district_id":"14833","city_id":"2752","sub_district":"โสกแสง","road":"สะอาดยิ่ง","soi":"ร่วมมิตร","moo":"12","floor":"8","separate_address":"1"},"custom_attributes":{"sub_district_id":{"attribute_code":"sub_district_id","value":"14833","label":"โสกแสง"},"city_id":{"attribute_code":"city_id","value":"2752","label":"นาจะหลวย"},"sub_district":{"attribute_code":"sub_district","value":"โสกแสง"}}},"paymentMethod":{"method":"cashondelivery","po_number":null,"additional_data":{"amgdpr_agreement":"{}"}}}`,
      {
        headers: {
          Authorization: "Bearer " + token,
          "content-type": "application/json",
        },
      }
    );
    check(response, {
      "status equals 200": response => response.status.toString() === "200",
    });
    sleep(2);
    //console.log(JSON.stringify(response))
  });

}

export function getCurrentCartDetail(token, domain) {
  let response;
  group('Cart Detail', function () {
    response = http.get(
      domain + "rest/V1/carts/mine/items/",
      {
        headers: {
          Authorization: "Bearer " + token,
          "content-type": "application/json",
        },
      }
    );
    sleep(2);
    //console.log(response.html().text())

  });
  return response.json();
}

export function deleteItem(token, domain, item) {
  let response;
  group('Delete Product', function () {
    response = http.del(
      domain + "rest/V1/carts/mine/items/" + item,
      "{}",
      {
        headers: {
          Authorization: "Bearer " + token,
          "content-type": "application/json",
        },
      }
    );
    check(response, {
      "status equals 200": response => response.status.toString() === "200",
    });
    sleep(2);

  });
  return response.json();
}

export function getShipppingInfomation(token, domain) {
  let response;
  group('ShipppingInfomation', function () {
    response = http.post(
      domain + "rest/V1/carts/mine/shipping-information",
      `{
            "addressInformation": {
                "shipping_address": {
                    "customerAddressId": "75",
                    "countryId": "TH",
                    "regionId": "871",
                    "regionCode": "NMA",
                    "region": "นครราชสีมา",
                    "customerId": "62",
                    "street": [
                        "111/110"
                    ],
                    "company": null,
                    "telephone": "0837336434",
                    "fax": null,
                    "postcode": "30000",
                    "city": "เมืองนครราชสีมา",
                    "firstname": "พัชรี",
                    "lastname": "โอฑาตะวงศ์",
                    "middlename": null,
                    "prefix": null,
                    "suffix": null,
                    "vatId": null,
                    "customAttributes": [
                        {
                            "attribute_code": "sub_district_id",
                            "value": "9513",
                            "label": "หนองบัวศาลา"
                        },
                        {
                            "attribute_code": "city_id",
                            "value": "2127",
                            "label": "เมืองนครราชสีมา"
                        },
                        {
                            "attribute_code": "sub_district",
                            "value": "หนองบัวศาลา"
                        },
                        {
                            "attribute_code": "moo",
                            "value": "10"
                        },
                        {
                            "attribute_code": "separate_address",
                            "value": "2",
                            "label": "Shipping"
                        }
                    ],
                    "extension_attributes": {
                        "sub_district_id": "9513",
                        "city_id": "2127",
                        "sub_district": "หนองบัวศาลา",
                        "moo": "10",
                        "separate_address": "2"
                    },
                    "custom_attributes": {
                        "sub_district_id": {
                            "attribute_code": "sub_district_id",
                            "value": "9513",
                            "label": "หนองบัวศาลา"
                        },
                        "city_id": {
                            "attribute_code": "city_id",
                            "value": "2127",
                            "label": "เมืองนครราชสีมา"
                        },
                        "sub_district": {
                            "attribute_code": "sub_district",
                            "value": "หนองบัวศาลา"
                        }
                    }
                },
                "billing_address": {
                    "customerAddressId": "76",
                    "countryId": "TH",
                    "regionId": "913",
                    "regionCode": "UBN",
                    "region": "อุบลราชธานี",
                    "customerId": "62",
                    "street": [
                        "111/110"
                    ],
                    "company": null,
                    "telephone": "0837336436",
                    "fax": null,
                    "postcode": "34280",
                    "city": "นาจะหลวย",
                    "firstname": "สมศรี",
                    "lastname": "วงศ์ศรีแก้ว",
                    "middlename": null,
                    "prefix": null,
                    "suffix": null,
                    "vatId": null,
                    "customAttributes": [
                        {
                            "attribute_code": "sub_district_id",
                            "value": "14833",
                            "label": "โสกแสง"
                        },
                        {
                            "attribute_code": "city_id",
                            "value": "2752",
                            "label": "นาจะหลวย"
                        },
                        {
                            "attribute_code": "sub_district",
                            "value": "โสกแสง"
                        },
                        {
                            "attribute_code": "road",
                            "value": "สะอาดยิ่ง"
                        },
                        {
                            "attribute_code": "soi",
                            "value": "ร่วมมิตร"
                        },
                        {
                            "attribute_code": "moo",
                            "value": "12"
                        },
                        {
                            "attribute_code": "floor",
                            "value": "8"
                        },
                        {
                            "attribute_code": "separate_address",
                            "value": "2",
                            "label": "Shipping"
                        }
                    ],
                    "extension_attributes": {
                        "sub_district_id": "14833",
                        "city_id": "2752",
                        "sub_district": "โสกแสง",
                        "road": "สะอาดยิ่ง",
                        "soi": "ร่วมมิตร",
                        "moo": "12",
                        "floor": "8",
                        "separate_address": "2"
                    },
                    "custom_attributes": {
                        "sub_district_id": {
                            "attribute_code": "sub_district_id",
                            "value": "14833",
                            "label": "โสกแสง"
                        },
                        "city_id": {
                            "attribute_code": "city_id",
                            "value": "2752",
                            "label": "นาจะหลวย"
                        },
                        "sub_district": {
                            "attribute_code": "sub_district",
                            "value": "โสกแสง"
                        }
                    }
                },
                "shipping_method_code": "home_delivery",
                "shipping_carrier_code": "home_delivery"
            }
        }`,
      {
        headers: {
          Authorization: "Bearer " + token,
          "content-type": "application/json",
        },
      }
    );
    check(response, {
      "status equals 200": response => response.status.toString() === "200",
    });
    sleep(2);
    //console.log(response.html().text())
    return response;
  });
}

export function estimateShippingAmount(token, domain) {
  let response;
  group('Shipping Amount', function () {
    response = http.post(
      domain + "rest/V1/carts/mine/estimate-shipping-methods-by-address-id",
      '{"addressId":"75"}',
      {
        headers: {
          Authorization: "Bearer " + token,
          "content-type": "application/json",
        },
      }
    );
    check(response, {
      "status equals 200": response => response.status.toString() === "200",
    });
    sleep(2);
    //console.log(JSON.stringify(response))
  });
}

export function setPayment(token, domain, cart_id) {
  let response;
  group('Set Payment', function () {
    response = http.post(
      domain + "rest/th/V1/carts/mine/set-payment-information",
      `{"cartId":"` + cart_id + `","paymentMethod":{"method":"cashondelivery","additional_data":{"amgdpr_agreement":"{}"}},"billingAddress":{"customerAddressId":"76","countryId":"TH","regionId":"913","regionCode":"UBN","region":"อุบลราชธานี","customerId":"62","street":["111/110"],"company":null,"telephone":"0837336436","fax":null,"postcode":"34280","city":"นาจะหลวย","firstname":"สมศรี","lastname":"วงศ์ศรีแก้ว","middlename":null,"prefix":null,"suffix":null,"vatId":null,"customAttributes":[{"attribute_code":"sub_district_id","value":"14833","label":"โสกแสง"},{"attribute_code":"city_id","value":"2752","label":"นาจะหลวย"},{"attribute_code":"sub_district","value":"โสกแสง"},{"attribute_code":"road","value":"สะอาดยิ่ง"},{"attribute_code":"soi","value":"ร่วมมิตร"},{"attribute_code":"moo","value":"12"},{"attribute_code":"floor","value":"8"},{"attribute_code":"separate_address","value":"1","label":"Billing"}],"extension_attributes":{"sub_district_id":"14833","city_id":"2752","sub_district":"โสกแสง","road":"สะอาดยิ่ง","soi":"ร่วมมิตร","moo":"12","floor":"8","separate_address":"1"},"custom_attributes":{"sub_district_id":{"attribute_code":"sub_district_id","value":"14833","label":"โสกแสง"},"city_id":{"attribute_code":"city_id","value":"2752","label":"นาจะหลวย"},"sub_district":{"attribute_code":"sub_district","value":"โสกแสง"}}}}`,
      {
        headers: {
          Authorization: "Bearer " + token,
          "content-type": "application/json",
        },
      }
    );
    check(response, {
      "status equals 200": response => response.status.toString() === "200",
    });
    sleep(2);
    //console.log(JSON.stringify(response))
  });

}

export function totalInfomation(token, domain) {
  let response;
  group('Total Infomation', function () {
    response = http.post(
      domain + "rest/th/V1/carts/mine/totals-information",
      `{"addressInformation":{"address":{"countryId":"TH","region":"อุบลราชธานี","regionId":"913","postcode":"34190","city":"วารินชำราบ","extension_attributes":{"advanced_conditions":{"custom_attributes":{"sub_district":{"attribute_code":"sub_district","value":"แสนสุข"},"city_id":{"attribute_code":"city_id","value":"2732"},"sub_district_id":{"attribute_code":"sub_district_id","value":"14772"}},"payment_method":null,"city":"วารินชำราบ","shipping_address_line":["555/44"],"billing_address_country":null,"currency":"THB"}}},"shipping_method_code":"home_delivery","shipping_carrier_code":"home_delivery"}}`,
      {
        headers: {
          Authorization: "Bearer " + token,
          "content-type": "application/json",
        },
      }
    );
    check(response, {
      "status equals 200": response => response.status.toString() === "200",
    });
    sleep(2);
    //console.log(JSON.stringify(response))
  });

}

export default function main() {

  let domain = `https://magento-app-sit.dohome.co.th/th/`
  //let token = login("patcharee.won@dohome.co.th","Patcharee**12345678",domain);
  let token = login("programmer-sap@dohome.co.th", "Patcharee**12345678", domain);
  // let cart_id = createEmptyCart(token,domain);
  let cart_info = getCurrentCartDetail(token, domain);
  //console.log(cart_info.length)
  //let has_product = false;
  for (let index = 0; index < cart_info.length; index++) {
    const element = cart_info[index];
    //console.log(element.item_id)
    deleteItem(token, domain, element.item_id)
    // has_product = true;
  }

  let cart_id = createEmptyCart(token, domain);
  let addproduct = addtocart(token, cart_id, '10002647', 1, domain);
  addproduct = addtocart(token, cart_id, '10003461', 1, domain);
  // addproduct = addtocart(token,cart_id,'10342504', 1,domain); //สินค้าไม่มี Stock
  getCurrentCartDetail(token, domain);
  getShipppingInfomation(token, domain)
  //estimateShippingAmount(token, domain)
  totalInfomation(token, domain)
  setPayment(token, domain, cart_id)
  totalInfomation(token, domain)
  payment(token, cart_id, domain)
}