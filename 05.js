import { sleep, group } from "k6";
import http from "k6/http";

export const options = {
  stages: [
    { duration: "10m", target: 1000 },
    // { duration: "6m", target: 250 },
    // { duration: "2m", target: 0 },
  ],
  ext: {
    loadimpact: {
      distribution: {
        "amazon:sg:singapore": {
          loadZone: "amazon:sg:singapore",
          percent: 100,
        },
      },
    },
  },
};

export default function main() {
  let response;

  group(
    "open_home_page - https://magento-app-sit.dohome.co.th/th/",
    function () {
      response = http.get("https://magento-app-sit.dohome.co.th/th/", {
        headers: {
          accept:
            "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
          "accept-encoding": "gzip, deflate, br",
          "accept-language":
            "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
          "cache-control": "max-age=0",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
          "sec-ch-ua-mobile": "?0",
          "sec-fetch-dest": "document",
          "sec-fetch-mode": "navigate",
          "sec-fetch-site": "cross-site",
          "sec-fetch-user": "?1",
          "upgrade-insecure-requests": "1",
        },
      });
      sleep(2.6);
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/modal/modal-popup.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/modal/modal-slide.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/modal/modal-custom.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/block-loader.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/tooltip/tooltip.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      sleep(3.8);
      response = http.get(
        "https://magento-app-sit.dohome.co.th/th/searchautocomplete/ajax/typeahead/?q=g8",
        {
          headers: {
            accept: "application/json, text/javascript, */*; q=0.01",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      sleep(3);
      response = http.get(
        "https://magento-app-sit.dohome.co.th/th/searchautocomplete/ajax/typeahead/?q=%E0%B9%80%E0%B8%84",
        {
          headers: {
            accept: "application/json, text/javascript, */*; q=0.01",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      sleep(6.4);
      response = http.get(
        "https://magento-app-sit.dohome.co.th/th/searchautocomplete/ajax/suggest/?q=%E0%B9%80%E0%B8%84%E0%B8%A3%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B8%9B%E0%B8%A3%E0%B8%B1%E0%B8%9A%E0%B8%AD%E0%B8%B2%E0%B8%81%E0%B8%B2%E0%B8%A8&store_id=1&cat=false",
        {
          headers: {
            accept: "application/json, text/javascript, */*; q=0.01",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      sleep(3.7);
    }
  );

  group(
    "search - https://magento-app-sit.dohome.co.th/th/catalogsearch/result/?q=%E0%B9%80%E0%B8%84%E0%B8%A3%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%87",
    function () {
      response = http.get(
        "https://magento-app-sit.dohome.co.th/th/catalogsearch/result/?q=%E0%B9%80%E0%B8%84%E0%B8%A3%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B8%9B%E0%B8%A3%E0%B8%B1%E0%B8%9A%E0%B8%AD%E0%B8%B2%E0%B8%81%E0%B8%B2%E0%B8%A8",
        {
          headers: {
            accept:
              "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "document",
            "sec-fetch-mode": "navigate",
            "sec-fetch-site": "same-origin",
            "upgrade-insecure-requests": "1",
          },
        }
      );
      sleep(14.8);
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/modal/modal-popup.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "if-modified-since": "Wed, 12 May 2021 01:03:44 GMT",
            "if-none-match": 'W/"609b2970-7e2"',
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/modal/modal-slide.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "if-modified-since": "Wed, 12 May 2021 01:03:44 GMT",
            "if-none-match": 'W/"609b2970-882"',
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/modal/modal-custom.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "if-modified-since": "Wed, 12 May 2021 01:03:44 GMT",
            "if-none-match": 'W/"609b2970-7d0"',
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/block-loader.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "if-modified-since": "Wed, 12 May 2021 01:03:44 GMT",
            "if-none-match": '"609b2970-14c"',
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/tooltip/tooltip.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "if-modified-since": "Wed, 12 May 2021 01:03:44 GMT",
            "if-none-match": '"609b2970-203"',
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/th/ammostviewed/analytics/view/?block_id=4&form_key=UiD1be3UcMuSoEGh",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      sleep(0.6);
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Theme/templates/breadcrumbs.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      sleep(0.5);
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_InstantPurchase/template/confirmation.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/mage/gallery/gallery.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      sleep(0.6);
      response = http.get(
        "https://magento-app-sit.dohome.co.th/th/gdprcookie/cookie/cookies?allowed=0",
        {
          headers: {
            accept: "application/json, text/javascript, */*; q=0.01",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/th/customer/section/load/?sections=amasty-storepickup-data&force_new_section_timestamp=false&_=1620786868819",
        {
          headers: {
            accept: "application/json, text/javascript, */*; q=0.01",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Amasty_GdprCookie/template/cookiebar.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Checkout/template/minicart/content.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_InstantPurchase/template/instant-purchase.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magenest_SocialLogin/template/modal_content.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/collection.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magenest_SocialLogin/template/authentication-popup.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/template/messages.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Captcha/template/checkout/captcha.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_ReCaptchaFrontendUi/template/reCaptcha.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
    }
  );
}
