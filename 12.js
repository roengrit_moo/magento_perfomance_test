import { sleep, check } from "k6";
import http from "k6/http";

export const options = {
  stages: [
    { duration: "2m", target: 250 },
    { duration: "6m", target: 250 },
    { duration: "2m", target: 0 },
  ],
  ext: {
    loadimpact: {
      distribution: {
        "amazon:sg:singapore": {
          loadZone: "amazon:sg:singapore",
          percent: 100,
        },
      },
    },
  },
};

export function login(username, password) {
  let response;
  let token;
  let domain = `https://magento-app-sit.dohome.co.th/th/`

  console.log('login');
  token = http.post(
    domain + "rest/V1/integration/customer/token",
    '{\r\n  "username": "' + username + '",\r\n  "password": "' + password + '"\r\n}'
    ,
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  check(token, {
    "status equals 200": response => response.status.toString() === "200",
  });
  if (token.status === 200) {
    //console.log(JSON.stringify(res.body));
    token = JSON.parse(token.body);
    console.log('token : ' + token);

  }


  return token;

}

export default function main() {
  let response;
  let token = login("programmer-sap@dohome.co.th", "Patcharee**12345678");
  response = http.get(
    "https://magento-app-sit.dohome.co.th/th/rest/th/V1/customers/me",
    {
      headers: {
        Authorization: "Bearer " + token,
        "content-type": "application/json",
      },
    }
  );
  check(response, {
    "status equals 200": response => response.status.toString() === "200",
  });
  sleep(2);
  //console.log(response.html().text());

}