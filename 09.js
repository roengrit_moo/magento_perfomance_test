import { check, group } from "k6";
import http from 'k6/http';
import { Rate } from 'k6/metrics';
import { sleep } from 'k6';

// See https://k6.io/docs/using-k6/options
export const options = {
  stages: [
    { duration: "2m", target: 50},
    { duration: "6m", target: 50},
    { duration: "2m", target: 0},
    //,
   // { duration: '6m', target: 250 },
   // { duration: '2m', target: 0 },
  ] 
}

export function login(username, password, domain) {
  let response;
  let token;

  console.log('login');
  token = http.post(
    domain + "rest/V1/integration/customer/token",
    '{\r\n  "username": "' + username + '",\r\n  "password": "' + password + '"\r\n}'
    ,
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  check(token, {
    "status equals 200": response => response.status.toString() === "200",
  });
  if (token.status === 200) {
    //console.log(JSON.stringify(res.body));
    token = JSON.parse(token.body);
    console.log('token : ' + token);

  }


  return token;

}


export function createEmptyCart(token, domain) {
  let response;
  console.log('create_cart');

  group('Create Empty Cart', function () {
    token = http.post(
      domain + "rest/V1/carts/mine",
      null,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      }
    );
    check(token, {
      "status equals 200": response => response.status.toString() === "200",
    });
    if (token.status === 200) {
      token = JSON.parse(token.body);
      console.log('q_id : ' + token)
    }
  });
  sleep(2);
  return token;

}

export function addtocart(token, cart_id, sku, qty, domain) {
  let response;
  group('Add Product', function () {
    response = http.post(
      domain + "rest/V1/carts/mine/items/",
      '{"cartItem":{"sku":"' + sku + '","qty":' + qty + ',"quote_id":"' + cart_id + '"}}',
      {
        headers: {
          Authorization: "Bearer " + token,
          "content-type": "application/json",
        },
      }
    );
    check(response, {
      "status equals 200": response => response.status.toString() === "200",
    });
    sleep(2);
  });
}

export function getCurrentCartDetail(token, domain) {
  let response;
  group('Cart Detail Befor Checkout', function () {
    response = http.get(
      domain + "rest/V1/carts/mine/items/",
      {
        headers: {
          Authorization: "Bearer " + token,
          "content-type": "application/json",
        },
      }
    );
    check(response, {
      "status equals 200": response => response.status.toString() === "200",
    });
    sleep(2);
    //console.log(response.html().text())

  });
  return response.json();
}

export function deleteItem(token, domain, item) {
  let response;
  group('Delete Product', function () {
    response = http.del(
      domain + "rest/V1/carts/mine/items/" + item,
      "{}",
      {
        headers: {
          Authorization: "Bearer " + token,
          "content-type": "application/json",
        },
      }
    );
    check(response, {
      "status equals 200": response => response.status.toString() === "200",
    });
    sleep(2);

  });
  return response.json();
}

export function getShipppingInfomation(token, domain) {
  let response;
  group('ShipppingInfomation', function () {
    response = http.post(
      domain + "rest/V1/carts/mine/shipping-information",
      `{"addressInformation":{"shipping_address":{"countryId":"TH","regionId":"913","regionCode":"UBN","region":"อุบลราชธานี","street":["555/44"],"telephone":"0982536161","postcode":"34190","city":"วารินชำราบ","firstname":"พัชรี","lastname":"วงศ์ศรีแก้ว","saveInAddressBook":1,"customAttributes":[{"attribute_code":"sub_district","value":"แสนสุข"},{"attribute_code":"floor","value":""},{"attribute_code":"moo","value":""},{"attribute_code":"soi","value":""},{"attribute_code":"road","value":""},{"attribute_code":"separate_address","value":2},{"attribute_code":"city_id","value":"2732"},{"attribute_code":"sub_district_id","value":"14772"}],"extension_attributes":{"sub_district":"แสนสุข","city_id":"2732","sub_district_id":"14772","floor":"","moo":"","soi":"","road":"","separate_address":2},"custom_attributes":{"sub_district":{"attribute_code":"sub_district","value":"แสนสุข"},"city_id":{"attribute_code":"city_id","value":"2732"},"sub_district_id":{"attribute_code":"sub_district_id","value":"14772"}}},"billing_address":{"countryId":"TH","regionId":"913","regionCode":"UBN","region":"อุบลราชธานี","street":["555/44"],"telephone":"0982536161","postcode":"34190","city":"วารินชำราบ","firstname":"พัชรี","lastname":"วงศ์ศรีแก้ว","saveInAddressBook":1,"customAttributes":[{"attribute_code":"sub_district","value":"แสนสุข"},{"attribute_code":"floor","value":""},{"attribute_code":"moo","value":""},{"attribute_code":"soi","value":""},{"attribute_code":"road","value":"555/44"},{"attribute_code":"city_id","value":"2732"},{"attribute_code":"sub_district_id","value":"14772"}],"extension_attributes":{"sub_district":"แสนสุข","city_id":"2732","sub_district_id":"14772","floor":"","moo":"","soi":"","road":"555/44"},"custom_attributes":{"sub_district":{"attribute_code":"sub_district","value":"แสนสุข"},"city_id":{"attribute_code":"city_id","value":"2732"},"sub_district_id":{"attribute_code":"sub_district_id","value":"14772"}}},"shipping_method_code":"freeshipping","shipping_carrier_code":"freeshipping"}}`,
      {
        headers: {
          Authorization: "Bearer " + token,
          "content-type": "application/json",
        },
      }
    );
    check(response, {
      "status equals 200": response => response.status.toString() === "200",
    });
    sleep(2);
    //console.log(response.html().text())
    return response;
  });
}

export function estimateShippingAmount(token, domain) {
  let response;
  group('Shipping Amount', function () {
    response = http.post(
      domain + "rest/th/V1/carts/mine/totals-information",
      `{"addressInformation":{"address":{"countryId":"TH","region":"อุบลราชธานี","regionId":"913","postcode":"34190","city":"วารินชำราบ","extension_attributes":{"advanced_conditions":{"custom_attributes":{"sub_district":{"attribute_code":"sub_district","value":"แสนสุข"},"city_id":{"attribute_code":"city_id","value":"2732"},"sub_district_id":{"attribute_code":"sub_district_id","value":"14772"}},"payment_method":null,"city":"วารินชำราบ","shipping_address_line":["555/44"],"billing_address_country":"TH","currency":"THB"}}},"shipping_method_code":"dohome","shipping_carrier_code":"dohome"}}`,
      {
        headers: {
          Authorization: "Bearer " + token,
          "content-type": "application/json",
        },
      }
    );
    check(response, {
      "status equals 200": response => response.status.toString() === "200",
    });
    sleep(2);
  });
}

export default function main() {

  let domain = `https://magento-app-sit.dohome.co.th/th/`
  let token = login("patcharee.won@dohome.co.th", "Patcharee**12345678", domain);
  // let cart_id = createEmptyCart(token,domain);
  let cart_info = getCurrentCartDetail(token, domain);
  //console.log(cart_info.length)
  //let has_product = false;
  for (let index = 0; index < cart_info.length; index++) {
    const element = cart_info[index];
    //console.log(element.item_id)
    deleteItem(token, domain, element.item_id)
    // has_product = true;
  }

  let cart_id = createEmptyCart(token, domain);
  let addproduct = addtocart(token, cart_id, '10176960', 1, domain);
  addproduct = addtocart(token, cart_id, '10190763', 1, domain);
  //addproduct = addtocart(token, cart_id, '10342504', 1, domain); //สินค้าไม่มี Stock
  getCurrentCartDetail(token, domain);
  getShipppingInfomation(token, domain)
  //estimateShippingAmount(token, domain)
}
