import { sleep, group } from "k6";
import http from "k6/http";

export const options = {
  stages: [
    { duration: "10m", target: 1000 },
    // { duration: "6m", target: 250 },
    // { duration: "2m", target: 0 },
  ],
  ext: {
    loadimpact: {
      distribution: {
        "amazon:sg:singapore": {
          loadZone: "amazon:sg:singapore",
          percent: 100,
        },
      },
    },
  },
};

export default function main() {
  let response;

  group(
    "open_home_page - https://magento-app-sit.dohome.co.th/th/",
    function () {
      response = http.get("https://magento-app-sit.dohome.co.th/th/", {
        headers: {
          accept:
            "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
          "accept-encoding": "gzip, deflate, br",
          "accept-language":
            "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
          "cache-control": "max-age=0",
          "sec-ch-ua":
            '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
          "sec-ch-ua-mobile": "?0",
          "sec-fetch-dest": "document",
          "sec-fetch-mode": "navigate",
          "sec-fetch-site": "cross-site",
          "sec-fetch-user": "?1",
          "upgrade-insecure-requests": "1",
        },
      });
      sleep(4.8);
    }
  );

  group(
    "goto_category_hardware - https://magento-app-sit.dohome.co.th/th/hardware-tool.html",
    function () {
      response = http.get(
        "https://magento-app-sit.dohome.co.th/th/hardware-tool.html",
        {
          headers: {
            accept:
              "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "document",
            "sec-fetch-mode": "navigate",
            "sec-fetch-site": "same-origin",
            "sec-fetch-user": "?1",
            "upgrade-insecure-requests": "1",
          },
        }
      );
      sleep(1.7);

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/modal/modal-popup.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/modal/modal-slide.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/modal/modal-custom.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/block-loader.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/tooltip/tooltip.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      sleep(1.2);

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Amasty_GdprCookie/template/cookiebar.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Checkout/template/minicart/content.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/collection.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magenest_SocialLogin/template/modal_content.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magenest_SocialLogin/template/authentication-popup.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/template/messages.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Captcha/template/checkout/captcha.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_ReCaptchaFrontendUi/template/reCaptcha.html",
        {
          headers: {
            accept: "*/*",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      sleep(1.3);

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/modal/modal-popup.html",
        {
          headers: {
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/modal/modal-slide.html",
        {
          headers: {
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/modal/modal-custom.html",
        {
          headers: {
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/block-loader.html",
        {
          headers: {
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/tooltip/tooltip.html",
        {
          headers: {
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/th/hardware-tool.html?p=2&is_scroll=1",
        {
          headers: {
            accept: "application/json, text/javascript, */*; q=0.01",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      sleep(0.6);

      response = http.get(
        "https://magento-app-sit.dohome.co.th/th/gdprcookie/cookie/cookies?allowed=0",
        {
          headers: {
            accept: "application/json, text/javascript, */*; q=0.01",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Amasty_GdprCookie/template/cookiebar.html",
        {
          headers: {
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.post(
        "https://magento-app-sit.dohome.co.th/th/xnotif/category/index/",
        {
          product: "null",
        },
        {
          headers: {
            accept: "application/json, text/javascript, */*; q=0.01",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
            origin: "https://magento-app-sit.dohome.co.th",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Checkout/template/minicart/content.html",
        {
          headers: {
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magenest_SocialLogin/template/modal_content.html",
        {
          headers: {
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/templates/collection.html",
        {
          headers: {
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magenest_SocialLogin/template/authentication-popup.html",
        {
          headers: {
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Ui/template/messages.html",
        {
          headers: {
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_Captcha/template/checkout/captcha.html",
        {
          headers: {
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );

      response = http.get(
        "https://magento-app-sit.dohome.co.th/static/version1620781381/frontend/Dohome/default/th_TH/Magento_ReCaptchaFrontendUi/template/reCaptcha.html",
        {
          headers: {
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      sleep(3.2);

      response = http.get(
        "https://magento-app-sit.dohome.co.th/th/customer/section/load/?sections=messages&force_new_section_timestamp=true&_=1620786163722",
        {
          headers: {
            accept: "application/json, text/javascript, */*; q=0.01",
            "accept-encoding": "gzip, deflate, br",
            "accept-language":
              "en-GB,en-US;q=0.9,en;q=0.8,th;q=0.7,zh-CN;q=0.6,zh;q=0.5",
            "sec-ch-ua":
              '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
            "sec-ch-ua-mobile": "?0",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-origin",
            "x-requested-with": "XMLHttpRequest",
          },
        }
      );
      sleep(1);
    }
  );
 
}