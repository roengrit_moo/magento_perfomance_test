import { sleep,check } from "k6";
import http from "k6/http";

export const options = {
  stages: [
    { duration: "2m", target: 100},
    { duration: "6m", target: 100},
    { duration: "2m", target: 0},
    // { duration: "6m", target: 500 },
    // { duration: "2m", target: 0 },
  ],
  ext: {
    loadimpact: {
      distribution: {
        "amazon:sg:singapore": {
          loadZone: "amazon:sg:singapore",
          percent: 100,
        },
      },
    },
  },
};

export default function main() {
  let response;

  response = http.get("https://magento-app-sit.dohome.co.th/th/");

  // Automatically added sleep
  console.log('process')
  sleep(10);
  check(response, {
    "status equals 200": response => response.status.toString() === "200",
  });
}
