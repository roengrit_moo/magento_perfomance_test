import { check, group } from "k6";
import http from 'k6/http';
import { Rate } from 'k6/metrics';
import { sleep } from 'k6';

// See https://k6.io/docs/using-k6/options
export const options = {
  stages: [
    { duration: "2m", target: 50},
    { duration: "6m", target: 50},
    { duration: "2m", target: 0},
    //,
   // { duration: '6m', target: 250 },
   // { duration: '2m', target: 0 },
  ] 
}
  

export function addtocart(token, sku, qty, domain) {
  let response;
  group('Add Product', function () {
    response = http.post(
      domain + "rest/V1/guest-carts/"+token+"/items",
      '{"cartItem":{"sku":"' + sku + '","qty":' + qty + '}}',
      {
        headers: {
          "content-type": "application/json",
        },
      }
    );
    check(response, {
      "status equals 200": response => response.status.toString() === "200",
    });
    sleep(2);
  });
}

   


export function CreateCart(domain) {
    let response;
    group('Create Cart', function () {
      response = http.post(
        domain + "rest/V1/guest-carts",
        ``,
        {
          headers: {
            "content-type": "application/json",
          },
        }
      );
      check(response, {
        "status equals 200": response => response.status.toString() === "200",
      });
      sleep(2);
    });
    let token = JSON.parse(response.body);
    console.log(token)
    return token;
  }


  export function getCurrentCartDetail(token, domain) {
    let response;
    group('Cart Detail Befor Checkout', function () {
      response = http.get(
        domain + "rest/V1/guest-carts/"+token+"/items",
        {
          headers: {
            "content-type": "application/json",
          },
        }
      );
      check(response, {
        "status equals 200": response => response.status.toString() === "200",
      });
      sleep(2);
      console.log(response.html().text())
  
    });
    return response.json();
  }

export default function main() {

  let domain = `https://magento-app-sit.dohome.co.th/th/`
  //let token = login("patcharee.won@dohome.co.th", "Patcharee**12345678", domain);
  // let cart_id = createEmptyCart(token,domain);
  //let cart_info = getCurrentCartDetail(token, domain);
  //console.log(cart_info.length)
  //let has_product = false;
  let cart = CreateCart(domain)
  let addproduct = addtocart(cart, '10176960', 1, domain);
  addproduct = addtocart(cart , '10190763', 1, domain);
  //console.log(JSON.parse(getCurrentCartDetail(cart,domain).body))
   
}
